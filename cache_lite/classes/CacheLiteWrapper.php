<?php

require_once elgg_get_plugins_path() . 'cache_lite/vendors/Cache_Lite/Cache/Lite.php';

/**
 * Wraps Cache_Lite class to define cache directory to be in elgg data dir and to avoid loading Pear library
 */
class CacheLiteWrapper extends Cache_Lite {

    /**
     * Set some default options
     * @param type $options
     */
    public function CacheLiteWrapper($options = array()) {
        // Define default values
        // More info about available options http://pear.php.net/manual/en/package.caching.cache-lite.cache-lite.cache-lite.php

        $default_options = array(
            'cacheDir' => elgg_get_data_path() . 'cache_lite/',
            //'lifeTime' => 3600,
            'automaticSerialization'  => true,
            'automaticCleaningFactor' => 20
        );

        // merge defaults with provided options
        $options = array_merge($options, $default_options);

        // Ensure that cache directory exists
        if (!is_dir($options['cacheDir'])) {
            @mkdir($options['cacheDir'], 0777, true);
        }

        // Construct Cache_Lite object
        parent::Cache_Lite($options);
    }

    /**
     * Redefine this function to avoid loading of Pear library
     * @param type $msg
     * @param type $code
     * @throws Exception
     */
    public function raiseError($msg, $code) {
        throw new Exception($msg, $code);
    }

}