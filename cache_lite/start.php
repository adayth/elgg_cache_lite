<?php

/* elgg_load_library('cache_lite');
  $cache = new CacheLiteWrapper();
  $cache->setLifeTime(10);

  if ($data = $cache->get(1, 1)) {
  elgg_dump('Data is cached');
  } else {
  elgg_dump('Data is not cached');
  $data = 'cached data lorem ipsum dolor';
  $cache->save($data, 1, 1);
  }
  elgg_dump($data); */
//elgg_dump($vars);

/**
 * Register cache_lite library
 */
function cache_lite_init() {
	elgg_register_library('cache_lite', elgg_get_plugins_path() . 'cache_lite/classes/CacheLiteWrapper.php');

    elgg_register_plugin_hook_handler('action', 'admin/site/flush_cache', 'cache_init_clean');
    elgg_register_event_handler('upgrade', 'system', 'cache_init_clean');
}

function cache_init_clean() {
    elgg_load_library('cache_lite');
    $cache = new CacheLiteWrapper();
    $cache->clean();
}

elgg_register_event_handler('init', 'system', 'cache_lite_init');